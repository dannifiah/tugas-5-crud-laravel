<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>CRUD Laravel</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('style/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ asset('style/assets/css/animate.min.css') }}" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="{{ asset('style/assets/css/paper-dashboard.css') }}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('style/assets/css/demo.css') }}" rel="stylesheet" />


    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('style/assets/css/themify-icons.css') }}" rel="stylesheet">

</head>
<body>
{{-- script JS --}}
<!--   Core JS Files   -->
<script src="{{ asset('style/assets/js/jquery-1.10.2.js') }}" type="text/javascript"></script>
<script src="{{ asset('style/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="{{ asset('style/assets/js/bootstrap-checkbox-radio.js') }}"></script>

<!--  Charts Plugin -->
<script src="{{ asset('style/assets/js/chartist.min.js') }}"></script>

<!--  Notifications Plugin    -->
<script src="{{ asset('style/assets/js/bootstrap-notify.js') }}"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="{{ asset('style/assets/js/paper-dashboard.js') }}"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('style/assets/js/demo.js') }}"></script>
{{-- end --}}

<div class="wrapper">
    <div class="sidebar" data-background-color="black" data-active-color="warning">
{{-- Tip 1: untuk merubah side bar menu menggunakan warna: data-background-color="white | black"
Tip 2: untuk merubah warna menu yg aktif menggunakan the data-active-color="primary | info | success | warning | danger" --}}

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="" class="simple-text">
                    CRUD Mahasiswa
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="/dashboard">
                        <i class="ti-dashboard"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active-pro">
                    <footer class="footer">
                        <div class="container-fluid">
                            <div class="copyright pull-right" style="margin-left: -5px">
                                &copy; 2021, Created by <a href="https://www.instagram.com/daniar.annifiah/" style="text-decoration:none; margin-right:20px; color:orange; background-color:transparent">Daniar</a>
                            </div>
                        </div>
                    </footer>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        @include('layout/navbar')
        @yield('content')
    </div>
</div>
</body>
</html>