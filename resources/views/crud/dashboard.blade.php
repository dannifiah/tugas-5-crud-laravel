@extends('layout.main')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card">
                    <div class="pull-left" style="margin-bottom: 10px; margin-top:10px;margin-left:5px;">
                        <h4 class="title" style="font-weight: bold">DAFTAR MAHASISWA</h4>
                    </div>
                    <div class="pull-right">
                        <a href="/create" class="btn btn-success btn-sm" style="margin-bottom: 10px; margin-top:10px; margin-right:5px;">
                            <i class="ti-plus">&ensp; Tambah Data</i>
                        </a>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-striped">
                            <thead style="background-color: orange;">
                                <tr>
                                    <th style="font-weight:bold">No</th>
                                    <th style="font-weight:bold">Nama</th>
                                    <th style="font-weight:bold">NIM</th>
                                    <th style="font-weight:bold">Kelas</th>
                                    <th style="font-weight:bold">Prodi</th>
                                    <th style="font-weight:bold">Fakultas</th>
                                    <th style="font-weight:bold" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($mahasiswa as $show)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $show->nama_mahasiswa }}</td>
                                        <td>{{ $show->nim_mahasiswa }}</td>
                                        <td>{{ $show->kelas_mahasiswa }}</td>
                                        <td>{{ $show->prodi_mahasiswa }}</td>
                                        <td>{{ $show->fakultas_mahasiswa }}</td>
                                        <td class="text-center">
                                            <a href="{{ url('update',$show->id) }}" class="btn btn-primary btn-sm">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            &ensp;
                                            <form action="{{ url('/dashboard',$show->id) }}" method="POST" onsubmit="return confirm('HAPUS DATA INI?')" style="display:inline">
                                                @method('delete')
                                                @csrf
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection