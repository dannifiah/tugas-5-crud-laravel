<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrudControllers extends Controller
{
    // show data
    public function show()
    {
        $mahasiswa = DB::table('mahasiswa')->get();
        return view('crud/dashboard', ['mahasiswa' => $mahasiswa]);
    }



    // deelete data
    public function deleting($id)
    {
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/dashboard')->with('status', 'Data Mahasiswa Telah Dihapus!');
    }



    // insert data
    public function create()
    {
        return view('crud/createdata');
    }
    public function created(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa'        => $request->nama,
            'nim_mahasiswa'         => $request->nim,
            'kelas_mahasiswa'       => $request->kelas,
            'prodi_mahasiswa'       => $request->prodi,
            'fakultas_mahasiswa'    => $request->fakultas
        ]);
        return redirect('/dashboard')->with('status', 'Data Mahasiswa Berhasil Ditambahkan!');
    }

    
    // ediit data
    public function update($id)
    {
        $updatingdata = DB::table('mahasiswa')->where('id', $id)->first();
        // @dd($updatingdata);
        return view('crud/updatedata', compact('updatingdata'));
    }

    public function updating(Request $request, $id)
    {
        DB::table('mahasiswa')->where('id', $id)->update([
            'nama_mahasiswa'        => $request->nama,
            'nim_mahasiswa'         => $request->nim,
            'kelas_mahasiswa'       => $request->kelas,
            'prodi_mahasiswa'       => $request->prodi,
            'fakultas_mahasiswa'    => $request->fakultas
        ]);
        return redirect('/dashboard')->with('status', 'Data Mahasiswa Berhasil Diubah!');
    }

}
