@extends('layout.main')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title" style="margin-bottom:30px; font-weight:bold; font-size:28px">Tambah Data Mahasiswa</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ url('/dashboard') }}" method="POST">
                                @csrf
                                    <div class="form-group" style="margin-left:20px; margin-right:10px">
                                        <label style="font-weight: bold; color:black" for="nama">Nama Mahasiswa</label>
                                        <input type="text" name="nama" class="form-control" style="border: solid 1px orange; background-color:rgb(255, 255, 220);
                                        " placeholder="masukan nama lengkap anda!">
                                    </div>
                                 
                                    <div class="form-group" style="margin-left:20px; margin-right:10px">
                                        <label style="font-weight: bold; color:black" for="nama">NIM Mahasiswa</label>
                                        <input type="text" name="nim" class="form-control" style="border: solid 1px orange; background-color:rgb(255, 255, 220)" placeholder="masukan nama lengkap anda!">
                                    </div>

                                    <div class="form-group" style="margin-left:20px; margin-right:10px">
                                        <label style="font-weight: bold; color:black" for="nama">Kelas Mahasiswa</label>
                                        <input type="text" name="kelas" class="form-control" style="border: solid 1px orange; background-color:rgb(255, 255, 220)" placeholder="masukan nama lengkap anda!">
                                    </div>

                                    <div class="form-group" style="margin-left:20px; margin-right:10px">
                                        <label style="font-weight: bold; color:black" for="nama">Prodi Mahasiswa</label>
                                        <input type="text" name="prodi" class="form-control" style="border: solid 1px orange; background-color:rgb(255, 255, 220)" placeholder="masukan nama lengkap anda!">
                                    </div>
                                 
                                    <div class="form-group" style="margin-left:20px; margin-right:10px">
                                        <label style="font-weight: bold; color:black" for="nama">Fakultas Mahasiswa</label>
                                        <input type="text" name="fakultas" class="form-control" style="border: solid 1px orange; background-color:rgb(255, 255, 220)" placeholder="masukan nama lengkap anda!">
                                    </div>
                                    <button type="submit" class="tombil btn btn-success" style="border-radius: 5px;margin-left:20px; margin-bottom: 10px"><i class="ti-save">&ensp;</i>Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection