<?php

use App\Http\Controllers\CrudControllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// show data
Route::get('/dashboard',[CrudControllers::class, 'show']);

// delete data
Route::delete('/dashboard/{id}',[CrudControllers::class, 'deleting']);

// tambah data
Route::get('/create',[CrudControllers::class, 'create']);
Route::post('/dashboard',[CrudControllers::class, 'created']);


// update data
Route::get('/update/{id}',[CrudControllers::class, 'update' ]);
Route::put('/dashboard/{id}',[CrudControllers::class, 'updating']);
